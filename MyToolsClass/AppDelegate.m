//
//  AppDelegate.m
//  testNetwork
//
//  Created by 常星 on 16/12/8.
//  Copyright © 2016年 常星. All rights reserved.
//

#import "AppDelegate.h"


#import "GuideView.h"

//#import "UMSocialData.h"
//#import "UMSocialWechatHandler.h"
//#import "UMSocialConfig.h"
//#import "UMSocialSnsPlatformManager.h"
//#import "UMSocialSnsService.h"
//#import "UMMobClick/MobClick.h"

#import <AudioToolbox/AudioToolbox.h>

#import <UserNotifications/UserNotifications.h>


@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    //设置角标为0，取消所有通知
//    [application setApplicationIconBadgeNumber:0];
//    [application cancelAllLocalNotifications];
//    
//    
//    _window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
//    [_window makeKeyAndVisible];
//    [self thirdInfo];
//    
//    [self checkVersion];
//    
//    
//    [self createTabBarController];
//    //引导页
//    [GuideView create];
    NSDictionary* dictionary = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (dictionary != nil)
    {
        NSLog(@"Launched from push notification: %@", dictionary);

    }
    return YES;
}

//检查版本，判断是否更新
- (void)checkVersion{




}
//创建tabbar视图
-(void)createTabBarController{


}

- (void)thirdInfo{
//    [AVOSCloud setApplicationId:AVOS_Id clientKey:AVOS_Key];
    
//    //UMeng 统计
//    UMConfigInstance.appKey = @"";//友盟的appkey
//    UMConfigInstance.channelId = nil;
//    UMConfigInstance.ePolicy = BATCH;
//#ifdef YYTest
//    [MobClick setAppVersion:@"0.1"];
//#else
//    [MobClick setAppVersion:VERSION];
//#endif
//    //友盟集成测试 发布需要设置yes
//    [MobClick setLogEnabled:NO];
//    [MobClick profileSignInWithPUID:@"-1"];
//    [MobClick startWithConfigure:UMConfigInstance];
//    
//    //友盟分享
//    [UMSocialData setAppKey:@""];
//    [UMSocialWechatHandler setWXAppId:@"" appSecret:@"" url:@""];
//    [UMSocialConfig hiddenNotInstallPlatforms:@[UMShareToWechatTimeline,UMShareToWechatSession]];
    
    //SV提示框配置
    [SVProgressHUD setMinimumDismissTimeInterval:1.5f];
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleCustom];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD setBackgroundLayerColor:[[UIColor blackColor] colorWithAlphaComponent:.3f]];
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    [SVProgressHUD setForegroundColor:[UIColor blackColor]];
    [SVProgressHUD setSuccessImage:[UIImage imageNamed:@"SVSuccess"]];
    [SVProgressHUD setErrorImage:[UIImage imageNamed:@"SVError"]];
    [SVProgressHUD setInfoImage:[UIImage imageNamed:@"SVSign"]];
    
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    [application setApplicationIconBadgeNumber:0];
    [application cancelAllLocalNotifications];
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     //[UMSocialSnsService  applicationDidBecomeActive];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
//    BOOL result = [UMSocialSnsService handleOpenURL:url];
//    if (result == FALSE) {
////        //调用其他SDK，例如支付宝SDK等
////        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
////            
////        }];
//        return YES;
//    }
//    
//    return result;
    
    
    return YES;
    
}

#pragma mark - Alipay
//以下两个方法,是应用之间跳转使用的方法,通过URL可以传递一些应用间的信息(另一个方法与友盟一起)
-(BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options{
    
//    if ([[url absoluteString] containsString:@""]) {
//        [UMSocialSnsService handleOpenURL:url wxApiDelegate:nil];
//        return YES;
//    }
    
//    [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
//        
//    }];
    
    return YES;
}


#pragma mark - UM
-(BOOL)isDirectShareInIconActionSheet
{
    return YES;
}

//-(void)didFinishGetUMSocialDataInViewController:(UMSocialResponseEntity *)response
//{
//    [SVProgressHUD dismiss];
//    
//    if(response.responseCode == UMSResponseCodeSuccess)
//    {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [SVProgressHUD showSuccessWithStatus:@"分享成功"];
//            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:ActPopView_ShareInvId];
//            [[NSUserDefaults standardUserDefaults] synchronize];
//        });
//    }else{
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [SVProgressHUD showSuccessWithStatus:@"分享失败"];
//        });
//    }
//}

#pragma mark - Notification
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
//    [AVOSCloud handleRemoteNotificationsWithDeviceToken:deviceToken];
//    
//    [[Single_ReceiveNotification shareInstance] updateDeviceTokenToService:[[AVInstallation currentInstallation] deviceToken]];


}

- (void)application:(UIApplication *)application
didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"x");
}

#pragma mark - receive NTF
- (void)handleNTFData:(NSDictionary *)userInfo{
    //单例文件处理通知事件的处理
    [[Single_ReceiveNotification shareInstance] receiveRemoteNotification:userInfo];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    //#ifdef YYTest
    //    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"message" message:[NSString stringWithFormat:@"%@",userInfo] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
    //    [alert show];
    //#endif
    //工程共保存音效文件
    NSURL *url=[[NSBundle mainBundle]URLForResource:@"14.wav" withExtension:nil];
    //2.加载音效文件，创建音效ID（SoundID,一个ID对应一个音效文件）
    SystemSoundID soundID=0;
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)url, &soundID);
    AudioServicesPlaySystemSound(soundID);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    
    [self handleNTFData:userInfo];
}


- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    NSDictionary *userInfo = notification.request.content.userInfo;
    if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [self handleNTFData:userInfo];
    }
    // 需要执行这个方法，选择是否提醒用户，有 Badge、Sound、Alert 三种类型可以选择设置
    completionHandler(UNNotificationPresentationOptionAlert);
}

/**
 * Required for iOS10+
 * 在后台和启动之前收到推送内容, 执行的方法
 */
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void (^)())completionHandler {
    NSDictionary * userInfo = response.notification.request.content.userInfo;
    if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [self handleNTFData:userInfo];
    }
    // 需要执行这个方法，选择是否提醒用户，有 Badge、Sound、Alert 三种类型可以选择设置
    completionHandler(UNNotificationPresentationOptionBadge|UNNotificationPresentationOptionSound);
}






#pragma mark - Orientation
-(UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
    
    if ([NSStringFromClass([[[window subviews]lastObject] class]) isEqualToString:@"videomarkView"]) {
        return UIInterfaceOrientationMaskLandscape;
        
    }
    return UIInterfaceOrientationMaskPortrait;
}



@end
