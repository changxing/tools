//
//  PublicMethod.h
//  jsb
//
//  Created by 常星 on 16/3/18.
//  Copyright © 2016年 常星. All rights reserved.
//

#import <Foundation/Foundation.h>

#define Public_SVShowSuccess \
[PublicMethod performBlock:.2 block:^{\
[SVProgressHUD showSuccessWithStatus:[info objectForKey:@"message"]];\
}];

#define Public_SVShowError \
[PublicMethod performBlock:.2 block:^{\
[SVProgressHUD showImage:nil status:[[[info objectForKey:@"errInfoList"] firstObject] objectForKey:@"errorDes"]];\
}];



#define ShowErrorMessage(a)     [SVProgressHUD showImage:nil status:a];


/**yyyy.MM.dd to 刚刚/几分钟前/几个月前*/
#define timeToC(timeStr) [PublicMethod time_getTimeSinceOldTime:[PublicMethod time_stringToDate:timeStr]]

@interface PublicMethod : NSObject


+ (NSString *)cachePath;

+ (NSString *)md5:(NSString *)str;

#pragma mark - delay
+(void)performBlock:(CGFloat)delay block:(block_void)block;
+ (void)performTimerBlock:(CGFloat)Interval timeOut:(CGFloat)timeOut eachBlock:(block_int)eachBlock completed:(block_void)completed;

#pragma mark - check
+ (NSString *)checkString:(NSString *)string;
+ (BOOL)checkTelNumber:(NSString *)telNumber;
+ (BOOL)checkEmail:(NSString *)email;
+ (BOOL)stringContainsEmoji:(NSString *)string;

#pragma mark - time
+ (NSDate *)time_getJetlag:(NSDate *)date;
+ (NSString *)time_getTimeSinceOldTime:(NSDate *)oldDate;
+ (NSDate *)time_stringToDate:(NSString *)dateStr;
+ (NSString *)time_DateToString:(NSDate *)date;
+ (NSString *)time_timeIntervalToStr:(NSTimeInterval)timeInterval;
+ (NSString *)showDateByTodayOrTomorrow:(NSString *)showDate;//今天的日期就显示“今天”，之前就显示日期


#pragma mark - constraints
+ (void)constraint_updata:(UIView *)obj type:(NSLayoutAttribute)type value:(CGFloat)value;
+ (NSLayoutConstraint *)constraint_updata:(UIView *)obj type:(NSLayoutAttribute)type;

+ (void)constraint_updata:(UIView *)searchView
                                firstItem:(UIView *)firstItem
                                     type:(NSLayoutAttribute)type
                                    value:(CGFloat)value;

+ (NSLayoutConstraint *)constraint_updata:(UIView *)searchView
                                firstItem:(UIView *)firstItem
                                     type:(NSLayoutAttribute)type;

+ (NSLayoutConstraint *)constraint_search:(UIView *)searchView
                                firstItem:(UIView *)firstItem
                                firstType:(NSLayoutAttribute)firstType
                               secondItem:(UIView *)secondItem
                               secondType:(NSLayoutAttribute)secondType;
#pragma mark - 计算内容文本的高度与宽度方法
+ (CGFloat)HeightForText:(NSString *)text withFontSize:(CGFloat)fontSize withTextWidth:(CGFloat)textWidth;
+ (CGFloat)WidthForText:(NSString *)text withFontSize:(CGFloat)fontSize withTextHeight:(CGFloat)textHeight;
+ (NSInteger)getToInt:(NSString*)strtemp;

+ (NSString* ) folderSizeAtPath:(NSString*) folderPath;
+ (void ) removeFilePath:(NSString *)path ;


@end
