//
//  PublicMethod.m
//  jsb
//
//  Created by 常星 on 16/3/18.
//  Copyright © 2016年 常星. All rights reserved.
//

#import "PublicMethod.h"
#import <CommonCrypto/CommonDigest.h>

@implementation PublicMethod


#pragma mark - path
+(NSString *)cachePath{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory,  NSUserDomainMask, YES);
    return [[paths objectAtIndex:0] stringByAppendingPathComponent:@"Caches"];
}

#pragma mark - md5
+ (NSString *)md5:(NSString *)str
{
    const char *cStr = [str UTF8String];
    unsigned char result[16];
    CC_MD5(cStr, (CC_LONG)strlen(cStr), result);
    return [NSString stringWithFormat:
            @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
}

#pragma mark - GCD
+ (void)performBlock:(CGFloat)delay block:(block_void)block
{
    CGFloat delayInSeconds = delay;
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), block);
}

+ (void)performTimerBlock:(CGFloat)Interval timeOut:(CGFloat)timeOut eachBlock:(block_int)eachBlock completed:(block_void)completed{
    
    
    __block CGFloat blockTimeOut = timeOut;
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0), Interval *NSEC_PER_SEC, 0); // set interval
    
    dispatch_source_set_event_handler(_timer, ^{
        
        if(blockTimeOut<=0){
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                completed();
            });
        }else{
            blockTimeOut--;
            dispatch_async(dispatch_get_main_queue(), ^{
                eachBlock(blockTimeOut);
            });
        }
    });
    dispatch_resume(_timer);
}


#pragma mark - 正则检测
+ (NSString *)checkString:(NSString *)string{
    
    NSMutableString *newStr = [NSMutableString stringWithString:[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]] ;
    
    
    while ([newStr containsString:@"\n\n"]) {
        newStr = [NSMutableString stringWithString:[newStr stringByReplacingOccurrencesOfString:@"\n\n" withString:@"\n"]];
    }
    
    return newStr;
}
+ (BOOL)checkTelNumber:(NSString *)telNumber
{
    if ([telNumber length]==11) {
        NSString *pattern = @"^1+[34578]+\\d{9}";
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
        BOOL isMatch = [pred evaluateWithObject:telNumber];
        return isMatch;
    }else
    {
        return NO;
    }
}



+ (BOOL)checkEmail:(NSString *)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}
#pragma mark - 判断是否含有emoji表情
+ (BOOL)stringContainsEmoji:(NSString *)string
{
    __block BOOL returnValue = NO;
    
    [string enumerateSubstringsInRange:NSMakeRange(0, [string length])
                               options:NSStringEnumerationByComposedCharacterSequences
                            usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
                                const unichar hs = [substring characterAtIndex:0];
                                if (0xd800 <= hs && hs <= 0xdbff) {
                                    if (substring.length > 1) {
                                        const unichar ls = [substring characterAtIndex:1];
                                        const int uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
                                        if (0x1d000 <= uc && uc <= 0x1f77f) {
                                            returnValue = YES;
                                        }
                                    }
                                } else if (substring.length > 1) {
                                    const unichar ls = [substring characterAtIndex:1];
                                    if (ls == 0x20e3) {
                                        returnValue = YES;
                                    }
                                } else {
                                    if (0x2100 <= hs && hs <= 0x27ff) {
                                        returnValue = YES;
                                    } else if (0x2B05 <= hs && hs <= 0x2b07) {
                                        returnValue = YES;
                                    } else if (0x2934 <= hs && hs <= 0x2935) {
                                        returnValue = YES;
                                    } else if (0x3297 <= hs && hs <= 0x3299) {
                                        returnValue = YES;
                                    } else if (hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030 || hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b || hs == 0x2b50) {
                                        returnValue = YES;
                                    }
                                }
                            }];
    
    return returnValue;
}
#pragma mark - time
+ (NSDate *)time_getJetlag:(NSDate *)date
{
    if (date == nil) {
        date = [NSDate date];
    }
    NSTimeZone *zone = [NSTimeZone systemTimeZone];
    NSInteger interval = [zone secondsFromGMTForDate:date];
    return [date dateByAddingTimeInterval:interval];
}

+ (NSString *)time_getTimeSinceOldTime:(NSDate *)oldDate
{
    NSDate *nowDate = [self time_getJetlag:nil];
    
    NSString *timeString=@"";
    
    NSTimeInterval cha = [nowDate timeIntervalSinceDate:oldDate];
    
    //分钟前  小时前  天前  日期
    
    if (cha < 60) {
        timeString = @"刚刚";
    }
    else if (cha < 3600)//minute
    {
        timeString=[NSString stringWithFormat:@"%ld分钟前", (unsigned long)(cha/60)];
    }
    else if (cha >= 3600 && cha < 86400) //hour
    {
        timeString=[NSString stringWithFormat:@"%ld小时前", (unsigned long)(cha/(60 * 60))];
    }
    else if (cha >= 86400 && cha < 864000) //day
    {
        timeString=[NSString stringWithFormat:@"%ld天前", (unsigned long)(cha/(60 * 60 * 24))];
    }else{
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setDateFormat:@"MM-dd"];
        timeString = [NSString stringWithFormat:@"%@",[format stringFromDate:oldDate]];
    }
    
    return timeString;
}

+ (NSDate *)time_stringToDate:(NSString *)dateStr{
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterLongStyle];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date=[formatter dateFromString:dateStr];
    
    NSDate *returnDate = [PublicMethod time_getJetlag:date];
    return returnDate;
}

+ (NSString *)time_DateToString:(NSDate *)date{
    if (date == nil) {
        date = [NSDate date];
    }
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterLongStyle];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    
    
    return [formatter stringFromDate:date];
}


+ (NSString *)time_timeIntervalToStr:(NSTimeInterval)timeInterval{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Shanghai"];
    [formatter setTimeZone:timeZone];
    
    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    NSString *confromTimespStr = [formatter stringFromDate:confromTimesp];
    return confromTimespStr;
}

+ (NSString *)showDateByTodayOrTomorrow:(NSString *)showDate{
    
    NSDate *today = [self getLocationDateBy:[NSDate date]];
    NSString *today_Str = [[NSString stringWithFormat:@"%@",today] substringToIndex:10];
    
    
    NSDate *tomorrow = [self getLocationDateBy:[[NSDate date] dateByAddingTimeInterval:24 * 60 * 60]];
    NSString *tomorrow_Str = [[NSString stringWithFormat:@"%@",tomorrow] substringToIndex:10];
    NSString *show_Str = [[NSString stringWithFormat:@"%@",showDate] substringToIndex:10];
    
    if ([show_Str isEqualToString:today_Str]) {
        return @"今天";
    }
    if ([show_Str isEqualToString:tomorrow_Str]) {
        return @"明天";
    }
//    return [show_Str substringFromIndex:5];
    return show_Str;
}
+ (NSDate *)getLocationDateBy:(NSDate *)anyDate
{
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];//或GMT
    NSTimeZone* destinationTimeZone = [NSTimeZone localTimeZone];
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:anyDate];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:anyDate];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    NSDate* destinationDateNow = [[NSDate alloc] initWithTimeInterval:interval sinceDate:anyDate];
    return destinationDateNow;
}

#pragma mark - constraints
+ (void)constraint_updata:(UIView *)obj type:(NSLayoutAttribute)type value:(CGFloat)value{
    [self constraint_updata:obj type:type].constant = value;
}

+ (NSLayoutConstraint *)constraint_updata:(UIView *)obj type:(NSLayoutAttribute)type{
    NSArray* constrains = obj.constraints;
    
    for (NSLayoutConstraint* constraint in constrains) {
        if (constraint.firstAttribute == type) {
            return constraint;
        }
    }
    return nil;
}

+ (void)constraint_updata:(UIView *)searchView
                firstItem:(UIView *)firstItem
                     type:(NSLayoutAttribute)type
                    value:(CGFloat)value{
    
    [self constraint_updata:searchView firstItem:firstItem type:type].constant = value;
}

+ (NSLayoutConstraint *)constraint_updata:(UIView *)searchView
                                firstItem:(UIView *)firstItem
                                     type:(NSLayoutAttribute)type{
    
    NSArray* constrains = searchView.constraints;
    
    for (NSLayoutConstraint* constraint in constrains) {
        
        if (constraint.firstItem == firstItem ) {
            if (constraint.firstAttribute == type) {
                //                constraint.constant = value;
                return constraint;
            }
        }
        
    }
    return nil;
}


+ (NSLayoutConstraint *)constraint_search:(UIView *)searchView
                                firstItem:(UIView *)firstItem
                                firstType:(NSLayoutAttribute)firstType
                               secondItem:(UIView *)secondItem
                               secondType:(NSLayoutAttribute)secondType{
    
    NSArray* constrains = searchView.constraints;
    
    for (NSLayoutConstraint* constraint in constrains) {
        
        if (constraint.firstItem == firstItem && constraint.secondItem == secondItem) {
            if (constraint.firstAttribute == firstType && constraint.secondAttribute == secondType) {
                return constraint;
            }
        }
        
    }
    
    for (NSLayoutConstraint* constraint in constrains) {
        
        if (constraint.firstItem == secondItem && constraint.secondItem == firstItem) {
            if (constraint.firstAttribute == secondType && constraint.secondAttribute == firstType) {
                return constraint;
            }
        }
        
    }
    
    
    return nil;
}


#pragma mark - 计算内容文本的高度方法
+ (CGFloat)HeightForText:(NSString *)text withFontSize:(CGFloat)fontSize withTextWidth:(CGFloat)textWidth
{
    // 获取文字字典
    NSDictionary *dict = @{NSFontAttributeName: [UIFont systemFontOfSize:fontSize]};
    // 设定最大宽高
    CGSize size = CGSizeMake(textWidth, 2000);
    // 计算文字Frame
    CGRect frame = [text boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dict context:nil];
    return frame.size.height;
}

#pragma mark - 计算内容文本的宽度方法
+ (CGFloat)WidthForText:(NSString *)text withFontSize:(CGFloat)fontSize withTextHeight:(CGFloat)textHeight
{
    NSDictionary *dict = @{NSFontAttributeName: [UIFont systemFontOfSize:fontSize]};
    CGSize size = CGSizeMake(2000, textHeight);
    CGRect frame = [text boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dict context:nil];
    return frame.size.width;
}

#pragma mark -得到中英文混合字符串长度
+ (NSInteger)getToInt:(NSString*)strtemp
{
    NSStringEncoding enc = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
    NSData* da = [strtemp dataUsingEncoding:enc];
    return [da length];
}
#pragma mark - 清除缓存的时候计算大小
+ (long long) fileSizeAtPath:(NSString*) filePath{
    NSFileManager* manager = [NSFileManager defaultManager];
    if ([manager fileExistsAtPath:filePath]){
        return [[manager attributesOfItemAtPath:filePath error:nil] fileSize];
    }
    return 0;
}
+ (NSString *) folderSizeAtPath:(NSString*) folderPath{
    NSFileManager* manager = [NSFileManager defaultManager];
    if (![manager fileExistsAtPath:folderPath]) return 0;
    NSEnumerator *childFilesEnumerator = [[manager subpathsAtPath:folderPath] objectEnumerator];
    NSString* fileName;
    long long folderSize = 0;
    while ((fileName = [childFilesEnumerator nextObject]) != nil){
        NSString* fileAbsolutePath = [folderPath stringByAppendingPathComponent:fileName];
        folderSize += [self fileSizeAtPath:fileAbsolutePath];
    }
    if (folderSize ==0) {
        return @"0M";
    }else if (folderSize/(1024.0*1024.0) >10)
    {
        return [NSString stringWithFormat:@"%.1fM",folderSize/(1024.0*1024.0)];
    }else
    {
        return [NSString stringWithFormat:@"%.2fM",folderSize/(1024.0*1024.0)];

    }
}

+(void) removeFilePath:(NSString *)path
{
    if([[NSFileManager defaultManager] fileExistsAtPath:path])//如果存在临时文件的配置文件
        
    {
        
        [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
        
    }
}



@end
