//
//  Contant.h
//  testNetwork
//
//  Created by 常星 on 16/12/9.
//  Copyright © 2016年 常星. All rights reserved.
//

#define VERSION [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]

//注意，关于 iOS10 系统版本的判断，可以用下面这个宏来判断。不能再用截取字符的方法。
#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)



#define MainColor UIColorFromRGB(0x4680d1)

#define ISIPhone4 (ScreenHeight<500)
#define ScaleX  (ISIPhone4 ? 1.0:ScreenWidth/320)
#define ScaleY  (ISIPhone4 ? 1.0:ScreenHeight/568)




#define ScreenHeight [[UIScreen mainScreen] bounds].size.height
#define ScreenWidth [[UIScreen mainScreen] bounds].size.width
#define StatusBarHeight 20
#define NavigationHeight 44
#define TabBarHeight 49.0

#define KeyWindow [[UIApplication sharedApplication] keyWindow]


#define TICK   NSDate *startTime = [NSDate date]
#define TOCK   NSLog(@"Time: %f", -[startTime timeIntervalSinceNow])

#define WeakSelf(weakSelf)  __weak __typeof(&*self)weakSelf = self;

#define ifString(string) if (string && [string length] > 0)

#define str(x) [NSString stringWithFormat:@"%ld",(unsigned long)x]

#define name_self @"app名称"
#define APPID @"1111111111"//appId


#define YYTest//注释此句即为正式环境
#ifdef YYTest

#pragma mark - 测试

#define Main_Path  @"http://192.168.0.254:80/jsb-router/api/rest/router"


#else

#pragma mark - 正式

#define Main_Path @"http://api.qcjsb.com/jsb-router/api/rest/router"




#endif

#pragma mark - other

#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

