//
//  Single_ReceiveNotification.m
//  jsb
//
//  Created by zhouyouyali on 16/6/17.
//  Copyright © 2016年 常星. All rights reserved.
//

#import "Single_ReceiveNotification.h"
#import "MYPostNetwork.h"


@interface Single_ReceiveNotification()


@end

@implementation Single_ReceiveNotification

+ (Single_ReceiveNotification *)shareInstance
{
    static dispatch_once_t onceToken = 0;
    static Single_ReceiveNotification *Instance = nil;
    dispatch_once(&onceToken, ^{
        Instance = [[Single_ReceiveNotification alloc] init];
    });
    
    
    return Instance;
}



- (void)updateDeviceTokenToService:(NSString *)deviceToken{
    
    if (![Account isLogin]) {
        return;
    }
    
    NSMutableDictionary *postData = [[NSMutableDictionary alloc] initWithCapacity:0];
    [postData setObject:deviceToken forKey:@"deviceToken"];
    [postData setObject:[Account token] forKey:@"sessionKey"];
    
    //登陆后，发送token保存到服务器
    [d_Url create:@"" postData:postData success:^(NSDictionary *info, NSString *type) {
        
    } error:^(NSDictionary *info, NSString *type) {
    }];
}



- (void)receiveRemoteNotification:(NSDictionary *)info{
    SingleNotificationType actionCode = (SingleNotificationType)[info[@"actionCode"] integerValue];
    NSString *showTest = info[@"title"];
    
    switch (actionCode) {
        case SingleNotification_OtherDeviceLogin:{
            NSString *useId = [info objectForKey:@"param"];
            
            if (![useId isEqualToString:[d_User userId]]) {
                return;
            }
            
            [SVProgressHUD dismiss];
            
            UIViewController *presentVC = [(AppDelegate *)[[UIApplication sharedApplication] delegate] presentVC];
            if (presentVC) {
                [presentVC dismissViewControllerAnimated:NO completion:nil];
            }
            
            UITabBarController *bar = [(AppDelegate *)[[UIApplication sharedApplication] delegate] tabBarController];
            
            
            [bar.viewControllers enumerateObjectsUsingBlock:^(__kindof UIViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([obj isKindOfClass:[UINavigationController class]]) {
                    
                    
                    [(UINavigationController *)obj setViewControllers:@[[(UINavigationController *)obj viewControllers].firstObject]];
                }
            }];
            
            [bar setSelectedIndex:0];
            
            
            [Account loginOut];
            
            [Account autoLogin:nil];
            
            [PublicMethod performBlock:.2f block:^{
                [SVProgressHUD showImage:nil status:@"您的账号正在其他设别上登录，您已被迫下线。"];
            }];
            
        }
            break;
        case SingleNotification_unreadMsg:
        {

        }
            break;
        case SingleNotification_answer:
        {

            
            [self showAlert:showTest];
        }
            break;
        case SingleNotification_replyByAnswer:
        {

          
            [self showAlert:showTest];
        }
            break;
        case SingleNotification_adoptAnswer:
        {

            [self showAlert:showTest];
        }
            break;
        case SingleNotification_newAnswer:
        {

            [self showAlert:showTest];
            break;
        }
            
        case SingleNotification_Invite:
        case SingleNotification_VSuccess:
        case SingleNotification_V_Article:
        case SingleNotification_RelpyFeedback:
        case SingleNotification_InviteVSuccess:
        {
            [self showAlert:showTest];
        }
            break;
        default:
            break;
    }
}

-(void)removeNotification:(SingleNotificationType)type value:(NSString *)value{
    
    if (type == SingleNotification_unreadMsg  ||
        type == SingleNotification_adoptAnswer) {
        return;
    }
    
    NSMutableDictionary *postValue = [[NSMutableDictionary alloc] initWithCapacity:0];
    [postValue setObject:[Account token] forKey:@"token"];
    
    if (type == SingleNotification_answer) {
        [postValue setObject:@"1" forKey:@"type"];
        [postValue setObject:value forKey:@"questionId"];
    }
    if (type == SingleNotification_replyByAnswer) {
        [postValue setObject:@"2" forKey:@"type"];
        [postValue setObject:value forKey:@"replyId"];
    }
//    [d_Url create:YYUrl_Ntf_Rm_QA postData:postValue success:nil error:nil];
}



-(void)showAlert:(NSString *)text{
    
    [KeyWindow setWindowLevel:UIWindowLevelAlert - 1];
    
    UILabel *alertView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, StatusBarHeight)];
    [alertView setFont:[UIFont boldSystemFontOfSize:12.f]];
    [alertView setText:text];
    [alertView setTextColor:[UIColor whiteColor]];
    [alertView setBackgroundColor:[UIColor blackColor]];
    [alertView setTextAlignment:NSTextAlignmentLeft];
    [KeyWindow addSubview:alertView];
    
    [PublicMethod performBlock:4.f block:^{
        [UIView animateWithDuration:.1f animations:^{
            [alertView setAlpha:0.f];
        } completion:^(BOOL finished) {
            [alertView removeFromSuperview];
        }];
        
        [KeyWindow setWindowLevel:0];
    }];
}




#pragma mark - launchWithNotification
-(void)launchWithNotification:(NSDictionary *)info{
    
    SingleNotificationType actionCode = (SingleNotificationType)[info[@"actionCode"] integerValue];
    
#ifdef YYTest
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"ntf" message:[NSString stringWithFormat:@"%@",info] delegate:nil cancelButtonTitle:@"enter" otherButtonTitles:nil, nil];
    [alert show];
#endif
    

    switch (actionCode) {
        case SingleNotification_answer:
        case SingleNotification_newAnswer:
        {

        }
            break;
        case SingleNotification_replyByAnswer:
        case SingleNotification_adoptAnswer:
        {

        }
            break;
        default:
            break;
    }
}

@end
