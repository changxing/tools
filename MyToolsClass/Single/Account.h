//
//  Account.h
//  jsb
//
//  Created by 常星 on 16/3/21.
//  Copyright © 2016年 常星. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const AccountLoginIn;
extern NSString *const AccountLoginOut;

@interface Account : NSObject

@property (nonatomic,copy) block_returnBool LoginSuccessBlock;

+ (Account *)shareInstance;


+(void)autoLogin:(block_void)success;
+(void)autoLogin:(block_void)success needShowView:(BOOL)needShowView;

+(void)loginByAccount:(NSString *)phoneNub
             password:(NSString *)password
              success:(block_dic_type)success
                error:(block_dic_type)error;
+(void)loginByWechat:(block_dic_type)success
               error:(block_dic_type)error;

+(void)userInfo:(block_void)success error:(block_void)error;



+(void)saveAccount:(NSString *)phoneNub
          password:(NSString *)password
             token:(NSString *)token;
+(void)cleanToken;

+(BOOL)isLogin;
+(void)loginOut;


+(NSString *)token;
+(NSString *)phoneNub;
+(NSString *)password;


+(NSString *)carLogoChange:(NSString *)logo;


+(void)bindingWX:(block_void)success;
@end
