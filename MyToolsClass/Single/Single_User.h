//
//  Single_User.h
//  testNetwork
//
//  Created by 常星 on 16/12/9.
//  Copyright © 2016年 常星. All rights reserved.
//

#define d_User [Single_User shareInstance]
#import <Foundation/Foundation.h>

@interface Single_User : NSObject

@property (nonatomic,strong) NSString *userName;
@property (nonatomic,strong) NSString *realName;
@property (nonatomic,strong) NSString *userId;
@property (nonatomic,strong) NSString *photo;
@property (nonatomic,strong) NSString *email;
@property (nonatomic,strong) NSString *phone;
@property (nonatomic,strong) NSString *introduce;
@property (nonatomic,strong) NSString *professional;
@property (nonatomic,strong) NSString *workYears;
@property (nonatomic,strong) NSString *goodBrand;
@property (nonatomic,strong) NSString *city;
@property (nonatomic,strong) NSString *qq;
@property (nonatomic,strong) NSString *weiXin;
@property (nonatomic,strong) NSString *followBrand;
@property (nonatomic,strong) NSString *replyNum;
@property (nonatomic,strong) NSString *askNum;
@property (nonatomic,strong) NSString *adoptNum;



+ (Single_User *)shareInstance;

-(void)loginOut;
-(void)loadUserInfo:(NSDictionary *)userInfo;



@end
