//
//  Account.m
//  jsb
//
//  Created by 常星 on 16/3/21.
//  Copyright © 2016年 常星. All rights reserved.
//

#import "Account.h"
#import "MYPostNetwork.h"
#import "SAMKeychain.h"
//#import "UMSocial.h"
#define KeyChainSer @"jsp.com.prts.zyyl"

NSString *const AccountLoginIn = @"AccountLoginIn";
NSString *const AccountLoginOut = @"AccountLoginOut";



@implementation Account
+ (Account *)shareInstance
{
    static dispatch_once_t onceToken = 0;
    static Account *Instance = nil;
    dispatch_once(&onceToken, ^{
        Instance = [[Account alloc] init];
        
    });
    
    return Instance;
}

+(void)autoLogin:(block_void)success{
    [self autoLogin:success needShowView:YES];
}

+(void)autoLogin:(block_void)success needShowView:(BOOL)needShowView{
    
    
    NSString *phoneNub = [self phoneNub];
    NSString *password = [self password];

    
    
    block_void errorBlock = ^(){

    };
//    不能根据phoneNub自动登录 否则踢出去的会继续登录
    if (!password || password.length == 0) {
        if (needShowView) {
            errorBlock();
        }
        return;
    }
    
    [self loginByAccount:phoneNub password:password success:^(NSDictionary *info, NSString *type) {
        if (success) {
            success();
        }
    } error:^(NSDictionary *info, NSString *type) {
        errorBlock();
    }];
}


+(void)loginByAccount:(NSString *)phoneNub
             password:(NSString *)password
              success:(block_dic_type)success
                error:(block_dic_type)error{
    
    
    NSMutableDictionary *postData = [NSMutableDictionary dictionaryWithCapacity:0];
    [postData setObject:phoneNub forKey:@"mobile"];
    [postData setObject:[PublicMethod md5:password] forKey:@"password"];
    [postData setObject:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"deviceModel"];
    [postData setObject:VERSION forKey:@"appVersion"];
    
  
#if TARGET_IPHONE_SIMULATOR//模拟器
    [postData setObject:@"123" forKey:@"deviceToken"];
#endif
    
    
    [d_Url create:@"denglu"
           postData:postData
            success:^(NSDictionary *info, NSString *type) {
                [self saveAccount:phoneNub password:password token:[info objectForKey:@"token"]];
                [[Single_User shareInstance] loadUserInfo:info[@"userInfo"]];
                success(info,type);
                [[NSNotificationCenter defaultCenter] postNotificationName:AccountLoginIn object:nil];
            } error:^(NSDictionary *info, NSString *type) {
                error(info,type);
            }];
}


+(void)loginByWechat:(block_dic_type)success
                error:(block_dic_type)error{

//    UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToWechatSession];
//    
//    
//    UITabBarController *delegate = [(AppDelegate *)([[UIApplication sharedApplication] delegate]) tabBarController];
//
//    snsPlatform.loginClickHandler(delegate,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
//        
//        if (response.responseCode == UMSResponseCodeSuccess) {
//            
//            UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary]valueForKey:UMShareToWechatSession];
//            
//            
//            NSMutableDictionary *postData = [NSMutableDictionary dictionaryWithCapacity:0];
//            [postData setObject:snsAccount.unionId forKey:@"unionId"];
//            [postData setObject:snsAccount.userName forKey:@"nickName"];
//            [postData setObject:snsAccount.iconURL forKey:@"logoImage"];
//            
//
//        
//            [d_Url create:@"dengluweixing" postData:postData success:^(NSDictionary *info, NSString *type) {
//                
//                [self saveAccount:@"" password:@"" token:[info objectForKey:@"token"]];
//                [[Single_User shareInstance] loadUserInfo:info[@"userInfo"]];
//                
//                
//                
//                [[Account shareInstance] LoginSuccessBlock]();
//                
//                if (success) {
//                    success(info,type);
//                }
//            } error:^(NSDictionary *info, NSString *type) {
//                Public_SVShowError
//                if (error) {
//                    error(info,type);
//                }
//            }];
//        }
//    });
}



#pragma mark - Info
+(void)userInfo:(block_void)success error:(block_void)error{
    
    [d_Url create:@"xinxin" postData:@{@"token" : [self token]} success:^(NSDictionary *info, NSString *type) {
        [[Single_User shareInstance] loadUserInfo:info[@"userInfo"]];
        success();
    } error:^(NSDictionary *info, NSString *type) {
        Public_SVShowError
        error();
    }];

    
    
}




#pragma mark - Local Persistence
+(void)saveAccount:(NSString *)phoneNub
          password:(NSString *)password
             token:(NSString *)token{
    
    if ([[SAMKeychain accountsForService:KeyChainSer] count] > 0) {
        NSString *count = [[[SAMKeychain accountsForService:KeyChainSer] firstObject] objectForKey:@"acct"];
        [SAMKeychain deletePasswordForService:KeyChainSer account:count];
    }

    [SAMKeychain setPassword:password forService:KeyChainSer account:phoneNub];
    
    //token 有效期无限就不处理  有限期则在取token时候 验证本地时间 主动清空token伪装token失效 ，则会自动用账号密码登录
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"token"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void)cleanToken{
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"token"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(BOOL)isLogin{
    return [[d_User userId] length] > 0 ? YES : NO;
}

+(void)loginOut{
    
    [d_User loginOut];
    
    [self cleanToken];
    
    NSString *count = [[[SAMKeychain accountsForService:KeyChainSer] firstObject] objectForKey:@"acct"];
    [SAMKeychain setPassword:@"" forService:KeyChainSer account:count];
    [[NSNotificationCenter defaultCenter] postNotificationName:AccountLoginOut object:nil];
    
}

#pragma mark - value
+(NSString *)token{
    NSString *token = [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
    if (token && token.length > 0) {
        return token;
    }
    return @"";
}

+(NSString *)phoneNub{
    NSString *account = [[[SAMKeychain accountsForService:KeyChainSer] firstObject] objectForKey:@"acct"];
    return account;
    
}

+(NSString *)password{
    return [SAMKeychain passwordForService:KeyChainSer account:[self phoneNub]];
}

+(NSString *)carLogoChange:(NSString *)logo
{
    NSString *codeUPP=[logo lowercaseString];

    return [NSString stringWithFormat:@"logo_%@",codeUPP];
}

#pragma mark - binding

+(void)bindingWX:(block_void)success{
    
//    UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToWechatSession];
//    
//    
//    UITabBarController *delegate = [(AppDelegate *)([[UIApplication sharedApplication] delegate]) tabBarController];
//    
//    
//    snsPlatform.loginClickHandler(delegate,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
//        
//        if (response.responseCode == UMSResponseCodeSuccess) {
//            
//            UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary]valueForKey:UMShareToWechatSession];
//            
//            NSMutableDictionary *postData = [NSMutableDictionary dictionaryWithCapacity:0];
//            [postData setObject:snsAccount.unionId forKey:@"unionId"];
//            [postData setObject:snsAccount.userName forKey:@"nickName"];
//            [postData setObject:snsAccount.iconURL forKey:@"logoImage"];
//            [postData setObject:[Account token] forKey:@"sessionKey"];
//        }
//    });
    
}

@end
