//
//  Single_User.m
//  testNetwork
//
//  Created by 常星 on 16/12/9.
//  Copyright © 2016年 常星. All rights reserved.
//

#import "Single_User.h"
#import "objc/runtime.h"

@implementation Single_User

+ (Single_User *)shareInstance
{
    static dispatch_once_t onceToken = 0;
    static Single_User *Instance = nil;
    dispatch_once(&onceToken, ^{
        Instance = [[Single_User alloc] init];

    });
    
    return Instance;
}
-(void)loadUserInfo:(NSDictionary *)userInfo{


}

/**
 退出登录时调用，删除已有的数据
 */
-(void)loginOut
{
    unsigned int numIvars; //成员变量个数
    //获取该类的所有属性名
    Ivar *vars = class_copyIvarList([self class], &numIvars);
    NSString *key=nil;
    for(int i = 0; i < numIvars; i++) {
        //得到每一个属性，key是每一个属性的名字
        Ivar thisIvar = vars[i];
        key = [NSString stringWithUTF8String:ivar_getName(thisIvar)];  //获取成员变量的名字
        [self setValue:@"" forKey:key];
    }
}


@end
