//
//  Single_ReceiveNotification.h
//  jsb
//
//  Created by zhouyouyali on 16/6/17.
//  Copyright © 2016年 常星. All rights reserved.
//

#import <Foundation/Foundation.h>


#define d_Ntf [Single_ReceiveNotification shareInstance]

typedef NS_ENUM(NSInteger, SingleNotificationType) {
    //异地登陆
    SingleNotification_OtherDeviceLogin = 11,
    //认证成功
    SingleNotification_VSuccess         = 12,
    //邀请成功
    SingleNotification_Invite           = 13,
    //邀请认证成功
    SingleNotification_InviteVSuccess   = 14,
    
    //有新的未读消息
    SingleNotification_unreadMsg        = 21,
    
    //我提出的问题有新的回答
    SingleNotification_answer           = 32,
    //我回答的问题有新的回复
    SingleNotification_replyByAnswer    = 33,
    //我的回答被采纳了
    SingleNotification_adoptAnswer      = 34,
    //我提出的问题有新的追答
    SingleNotification_newAnswer        = 35,
    
    //文章被加精
    SingleNotification_V_Article         = 41,
    //用户反馈被后台回复
    SingleNotification_RelpyFeedback     = 51,
    
};


@interface Single_ReceiveNotification : NSObject

@property (nonatomic,strong) UIWindow *alertWindow;



+ (Single_ReceiveNotification *)shareInstance;


- (void)updateDeviceTokenToService:(NSString *)deviceToken;


/**
 notifacate service
 */
- (void)receiveRemoteNotification:(NSDictionary *)info;
/**
 我提出的问题 SingleNotification_answer  value:questionId
 我回答的问题 SingleNotification_replyByAnswer  value:replyId
 */
-(void)removeNotification:(SingleNotificationType)type value:(NSString *)value;


/**
 点击通知启动
 */
-(void)launchWithNotification:(NSDictionary *)info;
@end
