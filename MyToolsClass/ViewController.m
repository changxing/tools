//
//  ViewController.m
//  MyToolsClass
//
//  Created by 常星 on 2017/2/14.
//  Copyright © 2017年 常星. All rights reserved.
//

#import "ViewController.h"
#import "Account.h"
#import "Single_User.h"
#import "Single_ReceiveNotification.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [Account shareInstance];
    [Single_User shareInstance];
    [Single_ReceiveNotification shareInstance];
    
    NSLog(@"1===%@",[Account shareInstance]);
    NSLog(@"2===%@",[Single_User shareInstance]);
    NSLog(@"3===%@",[Single_ReceiveNotification shareInstance]);

    
    NSLog(@"改动1");
    [self UI];
    // Do any additional setup after loading the view, typically from a nib.
}
-(void)UI{

    NSLog(@"改动2");

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
