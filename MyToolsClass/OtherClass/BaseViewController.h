//
//  BaseViewController.h
//  jsb
//
//  Created by 常星 on 16/3/17.
//  Copyright © 2016年 常星. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

+(void)resetNavigationBar:(UINavigationController *)nav;

-(void)addReturnBtn:(block_void)block willTap:(block_void)willTap dismiss:(BOOL)dissmiss alertMessage:(NSString *)message;
@end
