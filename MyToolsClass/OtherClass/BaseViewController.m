//
//  BaseViewController.m
//  jsb
//
//  Created by 常星 on 16/3/17.
//  Copyright © 2016年 常星. All rights reserved.
//

#import "BaseViewController.h"

#import "UIImage+Color.h"
#import "objc/runtime.h"

//#import "UMMobClick/MobClick.h"


@interface BaseViewController()<UIAlertViewDelegate>
@end

@implementation BaseViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self setUp];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //[MobClick beginLogPageView:NSStringFromClass([self class])];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    //[MobClick endLogPageView:NSStringFromClass([self class])];
}

-(void)presentViewController:(UIViewController *)viewControllerToPresent animated:(BOOL)flag completion:(void (^)(void))completion{
    [super presentViewController:viewControllerToPresent animated:flag completion:completion];
    
    UIViewController *presentVC = viewControllerToPresent;
    if ([viewControllerToPresent isKindOfClass:[UINavigationController class]]) {
        
        presentVC = [[viewControllerToPresent childViewControllers] firstObject];
        
    }
    
  
    
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] setPresentVC:presentVC];
    
}

-(void)dismissViewControllerAnimated:(BOOL)flag completion:(void (^)(void))completion{
    [super dismissViewControllerAnimated:flag completion:completion];
    
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] setPresentVC:nil];
}

-(void)setUp{
    
    //状态栏颜色
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    
    //计算从navigation下开始
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars = NO;
    self.modalPresentationCapturesStatusBarAppearance = NO;
   
    [BaseViewController resetNavigationBar:self.navigationController];
    
    
    //取消“返回”按钮文本
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
}

+(void)resetNavigationBar:(UINavigationController *)nav{
    
    //导航栏背景色
    [nav.navigationBar setBackgroundImage:[UIImage imageByColor:MainColor] forBarMetrics:UIBarMetricsDefault];
    nav.navigationBar.shadowImage = [UIImage imageByColor:MainColor];
    
    //导航栏不透明
    nav.navigationBar.translucent = NO;
    //导航栏标题文本颜色
    nav.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    
    //导航栏按钮颜色
    nav.navigationBar.barStyle = UIStatusBarStyleDefault;
    [nav.navigationBar setTintColor:[UIColor whiteColor]];
}

#pragma mark - Return Btn
-(void)addReturnBtn:(block_void)block willTap:(block_void)willTap dismiss:(BOOL)dissmiss alertMessage:(NSString *)message
{
//    UIImage *img = [UIImage imageNamed:!dissmiss ? @"ReturnBtn_White" : @"whiteReturn"];
    
    UIImage *img = [UIImage imageNamed:!dissmiss ? @"ReturnBtn_White" : @"ReturnBtn_White"];

    
    UIButton *btnLeft = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnLeft setFrame:CGRectMake(0, 0, 33, 33)];
    [btnLeft setImage:img forState:UIControlStateNormal];
    [btnLeft setTag:dissmiss];
    [btnLeft addTarget:self action:@selector(defaultReturnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    if (block) {
        objc_setAssociatedObject(btnLeft, @"tapBlock", block, OBJC_ASSOCIATION_COPY_NONATOMIC);
    }
    if (willTap) {
        objc_setAssociatedObject(btnLeft, @"willTapBlock", willTap, OBJC_ASSOCIATION_COPY_NONATOMIC);
    }
    if (message && ![message isEqualToString:@""]) {
        objc_setAssociatedObject(btnLeft, @"alertMessage", message, OBJC_ASSOCIATION_COPY_NONATOMIC);
    }
    
    
    
    UIBarButtonItem *backItemLeft=[[UIBarButtonItem alloc] initWithCustomView:btnLeft];
    
    
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc]
                               initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                               target:nil action:nil];
    spacer.width = -10;
    self.navigationItem.leftBarButtonItems = @[spacer, backItemLeft];
}

-(void)defaultReturnAction:(UIButton *)btn{
    
    
    
    block_void returnBlock = ^{
        block_void tapBlock = objc_getAssociatedObject(btn, @"tapBlock");
        if (tapBlock) {
            tapBlock();
        }
        
        if (btn.tag) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        else{
            [[self navigationController] popViewControllerAnimated:YES];
        }
    };
    
    block_void willTapBlock = objc_getAssociatedObject(btn, @"willTapBlock");
    if (willTapBlock) {
        willTapBlock();
    }

    
    NSString *alertMessage = objc_getAssociatedObject(btn, @"alertMessage");
    
    if (alertMessage) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:alertMessage delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        objc_setAssociatedObject(alert, @"returnBlock", returnBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
        [alert show];
        return;
    }
    
    returnBlock();
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0) {
        return;
    }
    block_void returnBlock = objc_getAssociatedObject(alertView, @"returnBlock");
    returnBlock();
}


-(BOOL)shouldAutorotate{

    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

@end
