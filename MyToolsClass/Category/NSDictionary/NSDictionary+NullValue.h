//
//  NSDictionary+EmptyValue.h
//  常星
//
//  Created by 常星 on 15/7/16.
//  Copyright (c) 2015年 常星. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (NSDictionary_NullValue)
- (id)iObjectForKey:(NSString *)key;
- (id)iObjectForKeyWithContant:(NSString *)key;

- (BOOL)iSetObject:(NSString *)aString forKey:(NSString *)aKey withBlock:(block_void)emptyValue;

-(void)iCopyItem:(NSDictionary *)dictionary byKeys:(NSArray *)keys;

/**
 通过数组中个key 顺序找到最后一层key代表的值丙炔转换为NSString(仅支持NSNumber or string)
 */
-(NSString *)iConvertObjectToStringBy:(id)keys ,...;
/**

 */
-(id)iObjectForKeys:(NSString *)keys,...;

- (void)iSetExistObject:(id)obj forKey:(NSString *)key;
@end
