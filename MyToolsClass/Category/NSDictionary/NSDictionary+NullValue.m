//
//  NSDictionary+EmptyValue.m
//  常星
//
//  Created by 常星 on 15/7/16.
//  Copyright (c) 2015年 常星. All rights reserved.
//

#import "NSDictionary+NullValue.h"


typedef NSString *(^block_Nub_returnStr)(NSNumber *numb);

@implementation NSDictionary (NSDictionary_NullValue)
-(id)iObjectForKeyWithContant:(NSString *)key{
    if ([[self allKeys] containsObject:key]) {
        return [self iObjectForKey:key];
    }
    return nil;
}

-(id)iObjectForKey:(NSString *)key
{
    id value = [self objectForKey:key];
    if (![value isEqual:[NSNull null]] &&
        value != nil) {
        return value;
    }
    return @"";
}

- (BOOL)iSetObject:(NSString *)aString forKey:(NSString *)aKey withBlock:(block_void)emptyValue{
    
    if (!aString || [aString isEqual:@""] || [aString isEqual:[NSNull null]]) {
        if (emptyValue) {
            emptyValue();
            return NO;
        }
        aString = @"";
    }
    
    [self setValue:aString forKey:aKey];
    return YES;
}
- (void)iSetExistObject:(id)obj forKey:(NSString *)key{
    if (obj) {
        [self setValue:obj forKey:key];
    }
}




#pragma mark - new

#define setVariableToArray(values)\
    NSMutableArray *iResult = [[NSMutableArray alloc] init];\
    va_list params; \
    va_start(params,values);\
    id arg;\
    if (values) {\
        id prev = values;\
        [iResult addObject:prev];\
        while( (arg = va_arg(params,id)) )\
        {\
            if ( arg ){\
                [iResult addObject:arg];\
            }\
        }\
    va_end(params);\
    }\

-(id)iObjectForKeys:(NSString *)keys,...{
    setVariableToArray(keys)
    
    id result = self;
    for (NSInteger i = 0; i < [iResult count]; ++i) {
        result = [result objectForKey:[iResult objectAtIndex:i]];
    }
    return result;
}

-(NSString *)iConvertObjectToStringBy:(id)keys ,...{
    setVariableToArray(keys)
    return [self convertObjectToStringBy:iResult];
}



-(NSString *)convertObjectToStringBy:(NSArray *)keys{
    id result = self;
    for (NSInteger i = 0; i < [keys count]; ++i) {
        result = [self convertObjectBy:[keys objectAtIndex:i] searchItem:result];
    }
    
    if (![result isKindOfClass:[NSString class]]) {
        NSAssert(0, @"need NSString");
    }
    return result;
}

-(id)convertObjectBy:(NSString *)aKey searchItem:(id)item{
    
    block_Nub_returnStr getStrByNub = ^(NSNumber *nub){
        NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
        return [numberFormatter stringFromNumber:nub];
    };
    
    id value;
    if ([item isKindOfClass:[NSDictionary class]]) {
        value = [item iObjectForKey:aKey];
    }else if ([item isKindOfClass:[NSString class]]){
        return item;
    }else if ([item isKindOfClass:[NSNumber class]]){
        return getStrByNub(item);
    }
    
    if ([value isKindOfClass:[NSNumber class]]) {
        return getStrByNub(value);
    }else if ([value isKindOfClass:[NSString class]] ||
              [value isKindOfClass:[NSDictionary class]]){
        return value;
    }else{
        NSAssert(0, @"need NSNumber,NSString,NSDictionary");
    }
    
    return value;
}

-(void)iCopyItem:(NSDictionary *)dictionary byKeys:(NSArray *)keys{
    
    [keys enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [self iSetObject:[dictionary iObjectForKey:obj] forKey:obj withBlock:nil];
    }];

}


@end
