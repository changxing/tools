//
//  UIButton+Animation.h
//  jsb
//
//  Created by 常星 on 16/3/25.
//  Copyright © 2016年 常星. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIButton (UIButton_Animation)

-(void)addScaleAnimation;

@end
