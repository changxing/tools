//
//  UIButton+Action.h
//  jsb
//
//  Created by zhouyouyali on 4/23/16.
//  Copyright © 2016 常星. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton(UIButton_Action)

-(void)addAction:(block_btn)action event:(UIControlEvents)event;

@end
