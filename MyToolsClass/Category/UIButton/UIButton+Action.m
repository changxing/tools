//
//  UIButton+Action.m
//  jsb
//
//  Created by zhouyouyali on 4/23/16.
//  Copyright © 2016 常星. All rights reserved.
//

#import "UIButton+Action.h"
#import "objc/runtime.h"

static NSString * YYButtonAction_iTapUpInSide = @"YYButtonAction_iTapUpInSide";

@implementation UIButton (UIButton_Action)

-(void)addAction:(block_btn)action event:(UIControlEvents)event{
    
    SEL iSEL;
    const void *mark;
    
    NSString *value;
    
    switch (event) {
        case UIControlEventTouchUpInside:
            iSEL = NSSelectorFromString(YYButtonAction_iTapUpInSide);
            mark = &YYButtonAction_iTapUpInSide;
            
            value = YYButtonAction_iTapUpInSide;
            break;
            
        default:
            break;
    }
    
    
    objc_setAssociatedObject(self, mark, action, OBJC_ASSOCIATION_COPY_NONATOMIC);
    [self addTarget:self action:iSEL forControlEvents:event];
    
}

-(void)YYButtonAction_iTapUpInSide{
    ((block_btn)(objc_getAssociatedObject(self, &YYButtonAction_iTapUpInSide)))(self);
}
@end
