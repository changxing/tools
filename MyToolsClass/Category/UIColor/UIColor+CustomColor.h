//
//  UIColor+CustomColor.h
//  常星
//
//  Created by 常星 on 15/6/25.
//  Copyright (c) 2015年 常星. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UIColor(UIColor_CustomColor)


+ (UIColor *)tabbarTextColor;

+ (UIColor *)customBlueColor;
+ (UIColor *)customGrayColor;
+ (UIColor *)customGreenColor;
+ (UIColor *)customLightGrayColor;
+ (UIColor *)customDarkGrayColor;


+ (UIColor *)fontColor;
+ (UIColor *)btnColor;
+ (UIColor *)btnDisableColor;
+ (UIColor *)btnHighlightColor;
+ (UIColor *)statusBarColor;

+ (UIColor *)colorWithRed:(NSUInteger)red
                    green:(NSUInteger)green
                     blue:(NSUInteger)blue;
@end
