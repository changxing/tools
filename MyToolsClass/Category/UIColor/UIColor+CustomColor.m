//
//  UIColor+CustomColor.m
//  常星
//
//  Created by 常星 on 15/6/25.
//  Copyright (c) 2015年 常星. All rights reserved.
//

#import "UIColor+CustomColor.h"

@implementation UIColor(UIColor_CustomColor)

+ (UIColor *)tabbarTextColor{
    return [self colorWithRed:109 green:171 blue:222];
    
}

+ (UIColor *)customBlueColor
{
    return [self colorWithRed:124 green:186 blue:252];
}

+ (UIColor *)customGreenColor
{
    return [self colorWithRed:104 green:180 blue:69];
}

+ (UIColor *)customGrayColor{
    return [self colorWithRed:245 green:245 blue:245];
}



+ (UIColor *)customLightGrayColor{
    return [self colorWithRed:231 green:231 blue:231];
}


+ (UIColor *)customDarkGrayColor{
    return [[self darkGrayColor] colorWithAlphaComponent:.5f];
}

+ (UIColor *)fontColor{
    return UIColorFromRGB(0x333333);
}

+ (UIColor *)btnColor{
    return MainColor;
}

+ (UIColor *)btnDisableColor{
    return UIColorFromRGB(0xcccccc);
}

+ (UIColor *)btnHighlightColor{
    return MainColor;
}

+ (UIColor *)statusBarColor{
    return MainColor;
}

+ (UIColor *)colorWithRed:(NSUInteger)red
                    green:(NSUInteger)green
                     blue:(NSUInteger)blue
{
    return [UIColor colorWithRed:(float)(red/255.f)
                           green:(float)(green/255.f)
                            blue:(float)(blue/255.f)
                           alpha:1.f];
}

@end
