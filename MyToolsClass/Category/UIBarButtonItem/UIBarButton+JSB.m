//
//  JSBBarButton.m
//  jsb
//
//  Created by zhouyouyali on 4/23/16.
//  Copyright © 2016 常星. All rights reserved.
//

#import "UIBarButton+JSB.h"
#import "UIButton+Action.h"

@implementation UIBarButtonItem(UIBarButton_JSB)

+(instancetype)buildByName:(NSString *)name action:(block_void)action{
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setBackgroundColor:MainColor];
    [btn setTitle:name forState:UIControlStateNormal];
    [[btn titleLabel] setFont:[UIFont systemFontOfSize:15.f]];
    
    [[btn layer] setCornerRadius:4.f];
    [[btn layer] setBorderColor:[UIColor whiteColor].CGColor];
    [[btn layer] setBorderWidth:1.f];
    
    [btn setClipsToBounds:YES];
    [btn sizeToFit];
    [btn setFrame:CGRectMake(0, 0, btn.frame.size.width + 24, 30)];
    
    [btn addAction:^(UIButton *btn) {
        if (action) {
            action();
        }
    } event:UIControlEventTouchUpInside];
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:btn];
    
    
    return item;

}



@end
