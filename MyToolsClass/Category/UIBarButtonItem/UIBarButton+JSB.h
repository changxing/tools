//
//  JSBBarButton.h
//  jsb
//
//  Created by zhouyouyali on 4/23/16.
//  Copyright © 2016 常星. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBarButtonItem (UIBarButton_JSB)

+(instancetype)buildByName:(NSString *)name action:(block_void)action;

@end
