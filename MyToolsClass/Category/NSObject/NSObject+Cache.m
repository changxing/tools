//
//  NSObject+Cache.m
//  jsb
//
//  Created by 周游亚力 on 16/4/11.
//  Copyright © 2016年 常星. All rights reserved.
//

#import "NSObject+Cache.h"
#import "objc/runtime.h"

static NSString *const YYNSObjectCache = @"YYNSObjectCache";

@implementation NSObject (NSObject_Cache)

#define isClass(iClass) [self isKindOfClass:[iClass class]]
#define iSelf(iClass) ((iClass *)self)

-(void)yl_firstLaunch{
    
    if (isClass(NSLayoutConstraint)) {
        CGFloat cacheValue = [iSelf(NSLayoutConstraint) constant];
        
        objc_setAssociatedObject(self, &YYNSObjectCache, [NSNumber numberWithFloat:cacheValue], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    
}

-(void)yl_setNewValue:(id)newValue{
    [self yl_firstLaunch];
    
    if (isClass(NSLayoutConstraint)) {
        [iSelf(NSLayoutConstraint) setConstant:[newValue floatValue]];
    }
}

-(void)yl_resume{
    [self yl_firstLaunch];
    if (isClass(NSLayoutConstraint)) {
        [iSelf(NSLayoutConstraint) setConstant:[objc_getAssociatedObject(self, &YYNSObjectCache) floatValue]];
    }
}
@end
