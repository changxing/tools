//
//  NSObject+Cache.h
//  jsb
//
//  Created by 周游亚力 on 16/4/11.
//  Copyright © 2016年 常星. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (NSObject_Cache)

-(void)yl_setNewValue:(id)newValue;
-(void)yl_resume;
@end
