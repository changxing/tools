//
//  UIImage+Color.m
//  jsb
//
//  Created by 常星 on 16/3/17.
//  Copyright © 2016年 常星. All rights reserved.
//

#import "UIImage+Color.h"

@implementation UIImage(UIImage_Color)
+(UIImage *)imageByColor:(UIColor *)color{
    CGRect rect=CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return theImage;
}
@end
