//
//  UIImage+Color.h
//  jsb
//
//  Created by 常星 on 16/3/17.
//  Copyright © 2016年 常星. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIImage(UIImage_Color)

+(UIImage *)imageByColor:(UIColor *)color;

@end
