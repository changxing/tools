//
//  UIImageView+Pinch.h
//  jsb
//
//  Created by 常星 on 16/5/5.
//  Copyright © 2016年 常星. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (Pinch)

-(void)addPinchGestureRecognizer;
@end
