//
//  UIImageView+Pinch.m
//  jsb
//
//  Created by 常星 on 16/5/5.
//  Copyright © 2016年 常星. All rights reserved.
//

#import "UIImageView+Pinch.h"
#import "objc/runtime.h"



static NSString *const CXUIImageView_Pinch= @"CXUIImageView_Pinch";
static NSString *const BOOLKEY = @"BOOLKEY";

@implementation UIImageView (Pinch)

-(void)addPinchGestureRecognizer
{
    UIPinchGestureRecognizer * pinch = objc_getAssociatedObject(self, &CXUIImageView_Pinch);
    if (!pinch) {
        pinch=[[UIPinchGestureRecognizer alloc]initWithTarget:self action:@selector(pinchClick:)];
        objc_setAssociatedObject(self, &CXUIImageView_Pinch, pinch, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    [self addGestureRecognizer:pinch];
}

-(void)pinchClick:(UIPinchGestureRecognizer *)pinch
{
    //图片的缩放与放大
    self.transform=CGAffineTransformScale(pinch.view.transform, pinch.scale, pinch.scale);
    //重置缩放系数，否则会累加
    pinch.scale=1.0;
}

@end
