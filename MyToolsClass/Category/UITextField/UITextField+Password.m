//
//  UITextField+Password.m
//  jsb
//
//  Created by 常星 on 16/4/18.
//  Copyright © 2016年 常星. All rights reserved.
//

#import "UITextField+Password.h"
#import "objc/runtime.h"


static NSString *const CXUITextField_viewBG = @"CXUITextField_viewBG";
static NSString *const BOOLKEY = @"BOOLKEY";


@implementation UITextField (Password)

-(void)addViewInputPasswordAndCount:(NSInteger )count
{
//    self.count=count;
    //需要增加一个VIEW,VIEW上再添加小view，和label显示*
    UIView * viewBG = objc_getAssociatedObject(self, &CXUITextField_viewBG);

    if (!viewBG) {
        viewBG = [[UIView alloc]initWithFrame:self.frame];
        objc_setAssociatedObject(self, &CXUITextField_viewBG, viewBG, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    [self.superview addSubview:viewBG];
    viewBG.backgroundColor=[UIColor whiteColor];
    viewBG.layer.borderWidth = 1;
    viewBG.layer.borderColor = [[UIColor customLightGrayColor] CGColor];
    

    for (int i=0; i<count-1; i++) {
        UIView *view=[[UIView alloc]initWithFrame:CGRectMake((1+i)*(self.frame.size.width*1.0/count), 0, 1, self.frame.size.height)];
        view.backgroundColor=[UIColor customLightGrayColor];
        [viewBG addSubview:view];
    }
    //    再创建输入文字后的小图
    for (int i=0; i<count; i++) {
        UILabel *labelview=[[UILabel alloc]initWithFrame:CGRectMake(0+i*(self.frame.size.width*1.0/count), 0, self.frame.size.width*1.0/count, self.frame.size.height)];
        labelview.text=@"*";
        labelview.font=[UIFont systemFontOfSize:16];
        labelview.textAlignment=1;
        labelview.tag=100+i;
        labelview.hidden=YES;
        [viewBG addSubview:labelview];
    }
    [self addTarget:self action:@selector(valuehidden) forControlEvents:UIControlEventAllEvents];
    

}



-(void)valuehidden
{
    
    NSInteger index=self.text.length;
    UIView * viewBG = objc_getAssociatedObject(self, &CXUITextField_viewBG);
    for (int i=0; i<index; i++) {
        UILabel *label=[viewBG viewWithTag:100+i];
        label.hidden=NO;
    }
    for (NSInteger i=index; i<10; i++) {
        UILabel *label=[viewBG viewWithTag:100+i];
        label.hidden=YES;
    }

}


@end
