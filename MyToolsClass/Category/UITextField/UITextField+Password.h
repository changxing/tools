//
//  UITextField+Password.h
//  jsb
//
//  Created by 常星 on 16/4/18.
//  Copyright © 2016年 常星. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (Password)
-(void)valuehidden;
-(void)addViewInputPasswordAndCount:(NSInteger )count;
@end
