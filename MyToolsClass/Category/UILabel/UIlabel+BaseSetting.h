//
//  UIlabel+LineSpacing.h
//  JSB
//
//  Created by 常星 on 15/9/9.
//  Copyright (c) 2015年 常星. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel(UIlabel_BaseSetting)

+(UILabel *)customLabel:(NSString *)text
               fontSize:(CGFloat)fontSize;

+(UILabel *)customLabel:(NSString *)text
               fontSize:(CGFloat)fontSize
              fillWidth:(CGFloat)fillWidth
             fillHeight:(CGFloat)fillHeight ;




-(NSMutableAttributedString *)setText:(NSString *)text keyWords:(NSString *)keyWords;


-(NSMutableParagraphStyle *)setText:(NSString *)text LineSpacing:(CGFloat)spacing;


#pragma mark - size
-(CGFloat)getHeight:(CGFloat)width;
-(CGFloat)getHeight:(CGFloat)width style:(NSMutableParagraphStyle *)style;
/**
 realHeight changeHeight
 */
-(CGSize)getHeightAndOffset:(CGFloat)width;
-(void)setText:(NSString *)text keyWordsArr:(NSArray *)keyWordsArr;

- (NSMutableParagraphStyle *)setFirstLineIndent:(CGFloat)indentValue;
@end
