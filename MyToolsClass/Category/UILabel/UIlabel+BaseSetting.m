//
//  UIlabel+LineSpacing.m
//  JSB
//
//  Created by 常星 on 15/9/9.
//  Copyright (c) 2015年 常星. All rights reserved.
//

#import "UIlabel+BaseSetting.h"

#import "PublicMethod.h"

@implementation UILabel(UIlabel_BaseSetting)

+(UILabel *)customLabel:(NSString *)text
               fontSize:(CGFloat)fontSize {
    return [self customLabel:text fontSize:fontSize fillWidth:0 fillHeight:0];
}

+(UILabel *)customLabel:(NSString *)text
               fontSize:(CGFloat)fontSize
              fillWidth:(CGFloat)fillWidth
             fillHeight:(CGFloat)fillHeight {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    [label setFont:[UIFont systemFontOfSize:fontSize]];
    [label setText:text];
    [label setTextColor:[UIColor fontColor]];
    [label setTextAlignment:NSTextAlignmentCenter];
    CGSize size = [label sizeThatFits:CGSizeMake(ScreenWidth, ScreenHeight)];
    
    [label setFrame:CGRectMake(0, 0, fillWidth > 0 ? fillWidth : size.width, fillHeight > 0 ? fillHeight : size.height)];
    return label;
}


-(NSMutableParagraphStyle *)setText:(NSString *)text LineSpacing:(CGFloat)spacing{
    
    NSMutableAttributedString * attributedString = [[NSMutableAttributedString alloc] initWithString:text];
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [text length])];
    [paragraphStyle setLineSpacing:spacing];
    [self setAttributedText:attributedString];
    
    return paragraphStyle;
}

-(NSMutableAttributedString *)setText:(NSString *)text keyWords:(NSString *)keyWords{
    return [self setText:text keyWords:keyWords color:MainColor];
}


-(NSMutableAttributedString *)setText:(NSString *)text keyWords:(NSString *)keyWords color:(UIColor *)color{
    color = [UIColor colorWithRed:253 green:136 blue:39];
    
    
    if (!keyWords || keyWords.length == 0) {
        [self setText:text];
        return [[NSMutableAttributedString alloc] initWithString:text];
    }
    
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:text];
    NSString *remainText = [NSString stringWithString:text];
    
    NSInteger rangeOffset = 0;
    while (keyWords.length != 0 &&
           remainText.length > keyWords.length) {
        
        NSRange range = [remainText rangeOfString:keyWords];
        
        if (range.length == 0) {
            remainText = @"";
            continue;
        }
        [attr addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(rangeOffset + range.location, range.length)];
        rangeOffset += (range.location + range.length);
        
        NSString *cacheText = [remainText substringFromIndex:range.location + range.length];
        remainText = cacheText;
    }
    [self setAttributedText:attr];
    
    return attr;
}

-(CGFloat)getHeight:(CGFloat)width{
    
    CGFloat realWidth = (width == -1 ? self.frame.size.width : width);
    CGFloat height = [[self text] boundingRectWithSize:CGSizeMake(realWidth, ScreenHeight) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[self font]} context:nil].size.height;
    
    
    return height;
}

-(CGFloat)getHeight:(CGFloat)width style:(NSMutableParagraphStyle *)style{
    
    CGFloat realWidth = (width == -1 ? self.frame.size.width : width);
    CGFloat height = [[self text] boundingRectWithSize:CGSizeMake(realWidth, ScreenHeight) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[self font],NSParagraphStyleAttributeName:style} context:nil].size.height;
    
    
    return height;
}


-(CGSize)getHeightAndOffset:(CGFloat)width{
    CGFloat lastHeight = self.frame.size.height;
    
    CGFloat height = [self getHeight:width];
    
    return CGSizeMake(height, height - lastHeight);
}
/**
 *  当需要让多个关键字变色的时候调用
 *
 *  @param text        完整的全部文字
 *  @param keyWordsArr 所有关键字所在的数组,关键字需要按照顺序放在数组中
 */
-(void)setText:(NSString *)text keyWordsArr:(NSArray *)keyWordsArr{
    if (!keyWordsArr || keyWordsArr.count == 0) {
        [self setText:text];
        return;
    }
    if (keyWordsArr.count==1) {
        [self setText:text keyWords:keyWordsArr.firstObject];
        return;
    }
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:text];
    NSInteger rangeOffset = 0;

    for (NSString *key in keyWordsArr) {
        if(text.length<rangeOffset)
        {
            break;
        }
        NSString *rangeStr=[text substringWithRange:NSMakeRange(rangeOffset, text.length-rangeOffset)];
        NSRange range=[rangeStr rangeOfString:key];
        if (range.length!=0) {
            [str addAttribute:NSForegroundColorAttributeName value:MainColor range:NSMakeRange(range.location+rangeOffset, range.length)];
            rangeOffset+=range.location+range.length;
        }
    }
    self.attributedText=str;

}

- (NSMutableParagraphStyle *)setFirstLineIndent:(CGFloat)indentValue{
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString :self.text];

    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc ] init];
    [paragraphStyle setFirstLineHeadIndent:indentValue];
    
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range : NSMakeRange(0 , [attributedString length])];
    
    [self setAttributedText:attributedString];
    
    return paragraphStyle;
}

@end
