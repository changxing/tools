//
//  UILabel+JSBIcon.m
//  jsb
//
//  Created by zhouyouyali on 16/5/30.
//  Copyright © 2016年 常星. All rights reserved.
//

#import "UILabel+JSBIcon.h"

@implementation UILabel(UIlabel_JSBIcon)

+(instancetype)buildJSBIcon:(UIColor *)backgroundColor
                       word:(NSString *)word
                  fontColor:(UIColor *)fontColor
                       side:(CGFloat)side
                       font:(UIFont *)font
{
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, side, side)];
    [label setFont:font];
    [label setText:word];
    [label setTextColor:fontColor];
    [label setBackgroundColor:backgroundColor];
    [label setTextAlignment:NSTextAlignmentCenter];
    
    [[label layer] setCornerRadius:4.f];
    [label setClipsToBounds:YES];
    
    return label;
}

@end
