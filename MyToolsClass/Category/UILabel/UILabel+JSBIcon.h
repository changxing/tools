//
//  UILabel+JSBIcon.h
//  jsb
//
//  Created by zhouyouyali on 16/5/30.
//  Copyright © 2016年 常星. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UILabel(UIlabel_JSBIcon)

+(instancetype)buildJSBIcon:(UIColor *)backgroundColor
                       word:(NSString *)word
                  fontColor:(UIColor *)fontColor
                       side:(CGFloat)side
                       font:(UIFont *)font;

@end
