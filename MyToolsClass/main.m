//
//  main.m
//  MyToolsClass
//
//  Created by 常星 on 2017/2/14.
//  Copyright © 2017年 常星. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
