//
//  MYPostNetwork.h
//  testNetwork
//
//  Created by 常星 on 16/12/8.
//  Copyright © 2016年 常星. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"



#define d_Url [MYPostNetwork shareInstance]

@interface MYPostNetwork : NSObject



+ (MYPostNetwork *) shareInstance;


-(void)create:(NSString *)type
     postData:(NSDictionary *)postData
      success:(block_dic_type)success
        error:(block_dic_type)error;


@end
