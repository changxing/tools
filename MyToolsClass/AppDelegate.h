//
//  AppDelegate.h
//  testNetwork
//
//  Created by 常星 on 16/12/8.
//  Copyright © 2016年 常星. All rights reserved.
//

#import <UIKit/UIKit.h>



#define ActPopView_ShareInvId @"InvitationActivity_1_0"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (weak, nonatomic) UIViewController *presentVC;
@property (strong, nonatomic) UITabBarController *tabBarController;

@end

