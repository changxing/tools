//
//  GuideView.m
//  jsb
//
//  Created by 常星 on 4/14/16.
//  Copyright © 2016 常星. All rights reserved.
//

#import "GuideView.h"

@interface GuideView ()
@end

@implementation GuideView

+ (void)create{
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"launch_1.0"]) {
        
        return;
    }
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"launch_1.0"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    NSInteger count = 3;
    
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    [scrollView setContentSize:CGSizeMake(ScreenWidth * 3, 0)];
    [scrollView setPagingEnabled:YES];
    [scrollView setBackgroundColor:[UIColor whiteColor]];
    [scrollView setTag:8765];
    
    for (NSInteger i = 0; i < count; ++i) {
        NSString *name = [NSString stringWithFormat:@"GuideView_%ld",(unsigned long)i + 1];
        
        
        UIImageView *view = [[UIImageView alloc] initWithFrame:CGRectMake(i * ScreenWidth, 0, ScreenWidth, ScreenHeight)];
        [view setImage:[UIImage imageNamed:name]];
        [view setUserInteractionEnabled:YES];
        [view setContentMode:UIViewContentModeScaleAspectFill];
        [scrollView addSubview:view];
        
        if (i == count - 1) {
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            [btn setBackgroundColor:[UIColor btnColor]];
            [btn setFrame:CGRectMake(0, 0, ScreenWidth * 0.7, 45 * 0.8)];
            [btn addTarget:self action:@selector(hidGuideView) forControlEvents:UIControlEventTouchUpInside];
            [btn setTitle:@"立即体验" forState:UIControlStateNormal];
          
            CGFloat scale = 0.77;
            if (ISIPhone4) {
                scale = 0.83;
            }
            [btn setCenter:CGPointMake(ScreenWidth/2, ScreenHeight * scale)];
            [[btn layer] setCornerRadius:btn.frame.size.height/2];
            [view addSubview:btn];

        }
    }
    [KeyWindow addSubview:scrollView];
}

+ (void)hidGuideView{
    UIScrollView *view = [KeyWindow viewWithTag:8765];
    
    [UIView animateWithDuration:.2f animations:^{
        [view setAlpha:0.f];
    } completion:^(BOOL finished) {
        [view removeFromSuperview];
    }];
}

@end
