//
//  GuideView.h
//  jsb
//
//  Created by 常星 on 4/14/16.
//  Copyright © 2016 常星. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GuideView : NSObject
//第一次启动后的提示页面的加载
+ (void)create;

@end
